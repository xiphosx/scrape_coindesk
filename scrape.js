const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');
const writeStream = fs.createWriteStream('post.csv');

// Write Headers
writeStream.write(`Title,Link,Date \n`);

request('https://www.coindesk.com/', (error, response, html) => {
    if (!error && response.statusCode == 200) {
        const $ = cheerio.load(html);

        $('.article-set a').each((index, element) => {
            const title = $(element)
                .attr('title')
                .replace(/\s\s+/g, '');
            const link = $(element).attr('href');
            const date = $(element)
                .find('.time')
                .text()
                .replace(/,/, '');

            // Write Row To CSV
            writeStream.write(`${title}, ${link}, ${date} \n`);
        });

        console.log('Scraping Done...');
    }
});